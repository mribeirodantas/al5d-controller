/*###################################################################
#                                                                   #
#       Código para controle de braço robótico al5d                 #
#    _____          _       _____   ______                          #
#   |_   _|        / \     |_   _|.' ____ \                         #
#     | |         / _ \      | |  | (___ \_|                        #
#     | |   _    / ___ \     | |   _.____`.                         #
#    _| |__/ | _/ /   \ \_  _| |_ | \____) |                        #
#   |________||____| |____||_____| \______.'                        #
#           Copyright (c) 2013                                      #
#   <mribeirodantas at lais.huol.ufrn.br>                           #
#                                                                   #
# This program is free software; you can redistribute it and/or     #
# modify it under the terms of the GNU General Public License       #
# as published by the Free Software Foundation; either version 3    #
# of the License, or (at your option) any later version.            #
#                                                                   #
# This program is distributed in the hope that it will be useful,   #
# but WITHOUT ANY WARRANTY; without even the implied warranty of    #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     #
# GNU General Public License for more details.                      #
#                                                                   #
# Read further in the LICENSE file.                                 #
#                                                                   #
###################################################################*/

// Definindo posição inicial dos servos 0,1,2,3 e 4
#define HOME_POS "#0P1500#1P1500#2P1500#3P1500#4P1500"
#define POS_INIC 1500
#define pi 3.14159265

#include<ncurses.h>
#include<stdio.h>
#include<stdlib.h>
#include <math.h>
#include "ufrn_al5d.h" 

int startServo(int servo);
int moverServo(int servo, int valor);

int posicoes[5] = { 1500, 1500, 1500, 1500, 1500 };
double angulos[4] = { 0.00,0.00, 0.00, 0.00};
double x = 0.00;
double parc1 = 0.00;
double parc2 = 0.00;
double parc3 = 0.00;
double parc4 = 0.00;
double parc5 = 0.00;
double parc6 = 0.00;
double parc7 = 0.00;
double parc8 = 0.00;
double parc9 = 0.00;
double parc10 = 0.00;
double parc11 = 0.00;
double y = 0.00;
double z = 0.00;
/* Estabelecendo conexão */
int serial_fd = 0;
char *string = NULL;
char *comando = NULL;
int servo = -1;



int main(void) {

    //angulos iniciais
   // angulos[0] = (POS_INIC-1450)*0.1;
    //angulos[1] = (POS_INIC-778)*0.14516;
    //angulos[2] = (-1*((POS_INIC-1690)*0.11538))-90;
    //angulos[3] = ((POS_INIC-1470)*0.0865);

    angulos[0] = (POS_INIC-1500)*0.1;
    angulos[1] = (POS_INIC-798)*0.14516;
    angulos[2] = (-1*((POS_INIC-1960)*0.09))-90;
    angulos[3] = ((POS_INIC-1470)*0.09);
 


/* Curses Initialisations */

    int ch;

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();

/* Inicialização do Manipulador Robótico */

   ufrn_header();

   string = (char*) malloc(sizeof(char)*BUFSIZE);

   printw("Controle do manipulador iniciado.\n");
   serial_fd = abrir_porta();
   if (!serial_fd) {
       printw("Nao foi possível se conectar a /dev/ttyS0");
       printw("\nVerifique a conexao serial.");
       return EXIT_FAILURE;
   } else {
       if (!configurar_porta(serial_fd)) {
           printw("Erro inicializando a porta\n");
           close(serial_fd);
           return EXIT_FAILURE;
       }
   }



    // Carregando a posicao inicial na string de envio
    sprintf(string,"%s",HOME_POS);

    // Posicionando o manipulador na posicao inicial
    //enviar_comando(string,serial_fd)==-1)
    if (!enviar_comando(string,serial_fd)) {
        printw("Houve um incidente durante o envio da string ao manipulador");
        return EXIT_FAILURE;
    }

    printw("Voce ja pode controlar o manipulador livremente.\n");

    // Comecando modo interativo
    printw("Pressione # para sair do modo interativo\n");
    printw("e ! para escolher o servo que deseja mudar.\n");

    while((ch = getch()) != '#') {
        switch(ch) {
            case '!':
		printw("\n0 - base");
		printw("\n1 - ombro");
		printw("\n2 - cotovelo");
		printw("\n3 - pulso");
		printw("\n4 - garra");
                printw("\nDigite um servo para controlar (0-4): ");
                servo = getch() - '0';
                if (startServo(servo)) {
                    printw("\nVocê já pode controlar o servo %d", servo);
                    printw("\ncom as setas direcionados do teclado");
                } else {
                    printw("\nEssa numeração não existe.");
                    printw("\nPressione ! para escolher novamente.");
                }
                break;
            case KEY_UP: //printw("\nSeta direcional para cima");
		posicoes[servo] = posicoes[servo] + 5;
                moverServo(servo, posicoes[servo]);
                break;
            case KEY_DOWN: //printw("\nSeta direcional para baixo");
		posicoes[servo] = posicoes[servo] - 5;
                moverServo(servo, posicoes[servo]);
                break;
            case KEY_LEFT: //printw("\nSeta direcional para a esquerda");
		posicoes[servo] = posicoes[servo] - 5;
                moverServo(servo, posicoes[servo]);
                break;
            case KEY_RIGHT: //printw("\nSeta direcional para a direita");
		posicoes[servo] = posicoes[servo] + 5;
                moverServo(servo, posicoes[servo]);
                break;
            case 'r':
                printw("Limpando a tela...\n");
                wclear(stdscr);
                break;
            default:
                {
                //  printw("\nA tecla pressionada foi: ");
                //  attron(A_BOLD);
                //  printw("%c", ch);
                //  attroff(A_BOLD);
                }
        }
    }

    refresh();
    endwin();
    close(serial_fd);

    printf("Ate mais!");

    return EXIT_SUCCESS;
}

int moverServo(int servo, int valor) {

    if (servo < 0 || servo > 4) {
        startServo(servo);
        return EXIT_FAILURE;
    } else {
       comando = (char*) malloc(sizeof(char)*BUFSIZE);
       serial_fd = abrir_porta();
       if (!serial_fd) {
           printw("Nao foi possível se conectar a /dev/ttyS0");
           printw("\nVerifique a conexao serial.");
           return EXIT_FAILURE;
       } else {
           if (!configurar_porta(serial_fd)) {
              printw("Erro inicializando a porta\n");
              close(serial_fd);
              return EXIT_FAILURE;
           }
       }

    switch(servo){
                
        case 0:
            
         // angulos[0] = -1*((valor-1450)*0.1);
            angulos[0] = -1*((valor-1500)*0.12);

            break;
        case 1:
           // angulos[1] = ((valor-778)*0.14516);
          angulos[1] = ((valor-798)*0.14516);

            break;
        case 2:
            //angulos[2] = (-1*((valor-1670)*0.11538))-90;
            angulos[2] = (-1*((valor-1670)*0.09))-90;
  
            break;
        case 3:
          // angulos[3] = ((valor-1470)*0.1065);
      angulos[3] = ((valor-1470)*0.09);
            break;
        case 4:
            break;
            
 
       }

parc1=cos(angulos[0] * pi/180)*cos(angulos[1] * pi/180)*cos(angulos[2] * pi/180)-cos(angulos[0] * pi/180)*sin(angulos[1] * pi/180)*sin(angulos[2] * pi/180) ;


parc2=-cos(angulos[0] * pi/180)*cos(angulos[1] * pi/180)*sin(angulos[2] * pi/180)  - cos(angulos[0] * pi/180)*sin(angulos[1] * pi/180)  *cos(angulos[2] * pi/180); 

parc3=cos(angulos[0] * pi/180)*cos(angulos[1] * pi/180)*cos(angulos[2] * pi/180)-cos(angulos[0] * pi/180)*sin(angulos[1] * pi/180)  *sin(angulos[2] * pi/180) ;

parc4=(cos(angulos[0] * pi/180)*cos(angulos[1] * pi/180));


x = 9.5*(cos(angulos[3] * pi/180)  * parc1 + sin(angulos[3] * pi/180)  * parc2) + 18.5 * parc3 + 14.5 * parc4;


parc5=(sin(angulos[0] * pi/180)*cos(angulos[1] * pi/180)*cos(angulos[2] * pi/180)-sin(angulos[0] * pi/180)*sin(angulos[1] * pi/180)*sin(angulos[2] * pi/180));

parc6=(-sin(angulos[0] * pi/180)*cos(angulos[1] * pi/180)*sin(angulos[2] * pi/180)-sin(angulos[0] * pi/180)*sin(angulos[1] * pi/180)*cos(angulos[2] * pi/180));  

parc7=(sin(angulos[0] * pi/180)*cos(angulos[1] * pi/180)*cos(angulos[2] * pi/180)-sin(angulos[0] * pi/180)*sin(angulos[1] * pi/180)*sin(angulos[2] * pi/180));

parc8= (sin(angulos[0] * pi/180)*cos(angulos[1] * pi/180));     

y=9.5*(cos(angulos[3] * pi/180)*parc5+sin(angulos[3] * pi/180)*parc6)+18.5*parc7+14.5*parc8;

parc9=(sin(angulos[1] * pi/180)*cos(angulos[2] * pi/180))+(sin(angulos[2] * pi/180)*cos(angulos[1] * pi/180));
parc10=(-sin(angulos[1] * pi/180)*sin(angulos[2] * pi/180))+(cos(angulos[1] * pi/180)*cos(angulos[2] * pi/180));
parc11=(sin(angulos[1] * pi/180)*cos(angulos[2] * pi/180))+(sin(angulos[2] * pi/180)*cos(angulos[1] * pi/180));

z=9.5*(cos(angulos[3] * pi/180)*parc9+sin(angulos[3] * pi/180)*parc10)+18.5*parc11+14.5*sin(angulos[1] * pi/180)+7.5;


	memset(comando, 0, BUFSIZE);
	sprintf(comando,"#%dP%d",servo,trava(servo,valor));
	enviar_comando(comando,serial_fd);
	printw("\nAngulo Base: %f \n -  Angulo Ombro: %f \n -  Angulo Cotovelo: %f \n -  Angulo Punho: %f \n",angulos[0],angulos[1],angulos[2],angulos[3]);
        //wclear(stdscr);
        printw("\nX = %f \n   Y = %f \n Z= %f \n", x,y,z);
        printw("\nPulso= %f \n", valor);

        return EXIT_SUCCESS;
    }
}

int startServo(int servo) {
    if (servo >= 0 && servo < 5) {
        return EXIT_FAILURE;
    } else {
        return EXIT_SUCCESS;

    }
}
