#include <opencv2/opencv.hpp>
#include <sstream>
#include <string>
#include <iostream>





/*void drawObject(int x, int y,IplImage &frame){
    
	//utilizando algumas das funções do OpenCv para desenhar a mira
	circle(frame,Point(x,y),20,Scalar(0,255,0),2);
	line(frame,Point(x,y-5),Point(x,y-25),Scalar(0,255,0),2);
	line(frame,Point(x,y+5),Point(x,y+25),Scalar(0,255,0),2);
	line(frame,Point(x-5,y),Point(x-25,y),Scalar(0,255,0),2);
	line(frame,Point(x+5,y),Point(x+25,y),Scalar(0,255,0),2);
    
	putText(frame,intToString(x)+","+intToString(y),Point(x,y+30),1,1,Scalar(0,255,0),2);
    
    
    
}
*/
IplImage* img = cvLoadImage("/Users/PabloHolanda/Desktop/fundo.png");

void mouseEvent(int evt, int x, int y, int flags, void* param){
    if(evt==CV_EVENT_LBUTTONDOWN){
        printf("\nAs coordenadas do mouse são: x= %d  y= %d", x,y);
        cvCircle(img, cvPoint(x,y), 20, cvScalar(0,255,0), 3);
        
    }
    
}


int main()
{
    
    //Exemplo de como por texto na imagem exibida na tela!
    CvFont font;
    double hScale=1.0;
    double vScale=1.0;
    int    lineWidth=2;
    cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX|CV_FONT_ITALIC, hScale,vScale,0,lineWidth);
    
    cvNamedWindow("Trabalho Robotica");
    while(1){
    //Atribui a função callback para eventos do mouse
    cvSetMouseCallback("Trabalho Robotica", mouseEvent, 0);
    
    //carrega a imagem no display
   
    //desenhar uma reta
    cvLine(img, cvPoint(100,100), cvPoint(450,100), cvScalar(0,255,0), 4);
    cvPutText(img,"Robotica",cvPoint(215,375),&font, cvScalar(0,0,0));
    //desenhar um circulo
    cvCircle(img, cvPoint(100,100), 20, cvScalar(0,255,0), 3);
    //desenhar um quadrilatero - 
    cvRectangle(img, cvPoint(100,250), cvPoint(450,500), cvScalar(255,0,0), 1);
    
    
    cvShowImage("Trabalho Robotica", img);
        char c = cvWaitKey(30);
        if (c == 27) {
            break;
        }

    //aguarda que alguma tecla seja pressionada pra encerrar o programa
  
    }
    cvDestroyWindow("Trabalho Robotica");
    cvReleaseImage(&img);


     
    //limpa a tela/memória
    
    
    return 0;
}
