EXEC	= controller
CXX	= gcc
MAIN	= controller.c
OPTIONS	= -O3 -Wall
LIBS	= -lufrn_lynx -lm -lncurses

main:	clean
	$(CXX) $(OPTIONS) $(MAIN) -I./include/ -L./lib/ $(LIBS) -o $(EXEC)

clean:
	clear
	rm -f include/*~
	rm -f lib/*~
	rm -f *~

